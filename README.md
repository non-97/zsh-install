# zsh install

## Reference
- [お前らのターミナルはダサい](https://qiita.com/kinchiki/items/57e9391128d07819c321)
- [zshを使ってみる](https://qiita.com/ryutoyasugi/items/cb895814d4149ca44f12)

## Usage
### install zsh
```
$ brew update
$ brew install zsh
```

### change default shell
```
// check zsh path
$ grep zsh /etc/shells

$ chsh -s /usr/local/bin/zsh
```

### Terminal restart

### install Prezto
```
$ git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

$ setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
```

### change config
```
$ cat ~/.bash_profile >> ~/.zshrc

$ vi ~/.zpreztorc
$ vi ~/.zshrc
```

### update config
```
$ source ~/.zpreztorc
$ source ~/.zshrc
```